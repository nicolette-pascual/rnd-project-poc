import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class HelperFunctions {
  static Future<BitmapDescriptor> customMapPin(String markerAsset) async {
    BitmapDescriptor customPin = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 1.5), markerAsset);
    return customPin;
  }
}
