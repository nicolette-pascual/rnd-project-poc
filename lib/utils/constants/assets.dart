class Assets {
  static const String DEFAULT_MARKER_ICON = 'images/default_marker.png';
  static const String SOURCE_MARKER_ICON = 'images/source_marker.png';
  static const String DESTINATION_MARKER_ICON = 'images/dest_marker.png';
}
