import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rnd_poc/models/map_marker.dart';
import 'package:rnd_poc/utils/constants/assets.dart';
import 'package:rnd_poc/widgets/google_map.dart';

class Home extends StatefulWidget {
  final String title;

  const Home({Key key, this.title}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
            child: MapWidget(
          // add location coordinate/s as a list
          // Note: the first and last coordinates will be considered as
          // source location and destination location respectively

          // if only one coordinate is given, it will still be considered as
          // the source location
          coordinates: [
            MapMarker(
                coordinate: LatLng(45.521563, -122.677433),
                markerAsset: Assets.SOURCE_MARKER_ICON,
                title: "Source Location"),
            // If markerAsset is not specified, DEFAULT_MARKER_ICON is used by default as the custom pin icon
            MapMarker(coordinate: LatLng(46.122, -122), title: "Middle Point"),
            MapMarker(
                coordinate: LatLng(46, -122.677433),
                markerAsset: Assets.DESTINATION_MARKER_ICON,
                title: "Destination Location"),
          ],
        )));
  }
}
