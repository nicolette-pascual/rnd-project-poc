import 'package:flutter/material.dart';
import 'package:rnd_poc/models/map_marker.dart';
import 'package:rnd_poc/utils/constants/assets.dart';

class CustomInfoWindow extends StatefulWidget {
  final double windowPosition;
  final MapMarker mapMarker;

  const CustomInfoWindow({Key key, this.windowPosition, this.mapMarker})
      : super(key: key);

  @override
  _CustomInfoWindowState createState() => _CustomInfoWindowState();
}

class _CustomInfoWindowState extends State<CustomInfoWindow> {
  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      bottom: widget.windowPosition,
      left: 0,
      right: 0,
      duration: Duration(milliseconds: 300),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: <BoxShadow>[
              BoxShadow(
                  blurRadius: 20,
                  offset: Offset.zero,
                  color: Colors.grey.withOpacity(0.5))
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              children: <Widget>[
                SizedBox(
                    height: 50,
                    width: 50,
                    child: Image.asset(widget.mapMarker?.markerAsset ??
                        Assets.DEFAULT_MARKER_ICON)),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: (widget.mapMarker != null)
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(widget.mapMarker?.title ?? '[Title]',
                                style: TextStyle(fontWeight: FontWeight.w400)),
                            Text("Latitude: " +
                                    widget.mapMarker?.coordinate?.latitude
                                        ?.toString() ??
                                ''),
                            Text("Longitude: " +
                                    widget.mapMarker?.coordinate?.longitude
                                        ?.toString() ??
                                ''),
                          ],
                        )
                      : Container(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
