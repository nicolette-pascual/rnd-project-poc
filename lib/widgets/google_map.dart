import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rnd_poc/models/map_marker.dart';
import 'package:rnd_poc/utils/constants/assets.dart';
import 'package:rnd_poc/utils/constants/helper_functions/helper_functions.dart';
import 'package:rnd_poc/widgets/custom_info_window.dart';

class MapWidget extends StatefulWidget {
  final double cameraZoom;
  final List<MapMarker> coordinates;

  const MapWidget({Key key, this.cameraZoom, this.coordinates})
      : assert(coordinates != null),
        super(key: key);

  @override
  _MapWidgetState createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  // manages camera function (position, animation, zoom)
  // similar to other controllers available in Flutter
  Completer<GoogleMapController> _mapController = Completer();

  final Set<Marker> _markers = {};

  LatLng _sourceLoc;
  LatLng _destinationLoc;

  // for showing custom info window
  double _windowPosition = -200;
  MapMarker _currentMapMarker;

  @override
  void initState() {
    // set the first coordinate in the list the "source location"
    // and the last coordinate as the "destination"
    _sourceLoc = widget.coordinates.first.coordinate;
    _destinationLoc = widget.coordinates.last.coordinate;
    super.initState();
  }

  _initLocations() {
    List<MapMarker> _coordinateList = widget.coordinates ?? [];

    for (var _coordinate in _coordinateList) {
      _addMarker(
          marker: _coordinate,
          markerId: _coordinateList.indexOf(_coordinate).toString());
    }
  }

  // required parameter that sets the starting camera position
  // Camera position describes which part of the world you want the map to point at.
  _initialCameraPosition(LatLng _center) {
    return CameraPosition(
      target: _center,
      zoom: widget.cameraZoom ?? 8.0,
    );
  }

  // method that is called on map creation and takes a MapController as a parameter
  void _onMapCreated(GoogleMapController controller) {
    _mapController.complete(controller);
    setState(() {
      _initLocations();
    });
  }

  void _addMarker({MapMarker marker, String markerId}) async {
    String _iconAsset = marker.markerAsset ?? Assets.DEFAULT_MARKER_ICON;

    _markers.add(Marker(
        // marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(markerId ?? ''),
        position: marker.coordinate,
        icon: await HelperFunctions.customMapPin(_iconAsset),
        onTap: () {
          setState(() {
            _currentMapMarker = marker;
            _windowPosition = 0;
          });
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: _initialCameraPosition(_sourceLoc),
          markers: _markers,
          // tap anywhere in the Map to close custom info window
          onTap: (LatLng location) {
            setState(() {
              _windowPosition = -200;
            });
          },
        ),
        CustomInfoWindow(
          windowPosition: _windowPosition,
          mapMarker: _currentMapMarker,
        )
      ],
    );
  }
}
