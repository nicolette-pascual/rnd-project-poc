import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapMarker {
  @required
  LatLng coordinate;
  String markerAsset;
  String title;
  String subtitle;

  MapMarker({this.coordinate, this.markerAsset, this.title, this.subtitle});
}
